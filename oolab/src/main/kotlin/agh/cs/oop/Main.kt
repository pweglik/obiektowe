package agh.cs.oop

import  agh.cs.oop.gui.SimulationGUI
import androidx.compose.foundation.layout.Row
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.singleWindowApplication


@OptIn(ExperimentalComposeUiApi::class)
fun main() = singleWindowApplication(
    title = "Life and Death - The simulation", state = WindowState(size = DpSize(2000.dp, 1000.dp))
) {
    Row() {
        SimulationGUI(1)

        SimulationGUI(2)
    }
}