package agh.cs.oop.mapElements

import agh.cs.oop.map.MapDirection
import agh.cs.oop.map.RectMapWithJungle
import agh.cs.oop.utils.MoveDirection
import agh.cs.oop.utils.Vector2d
import java.time.Instant
import java.time.format.DateTimeFormatter

class Animal(private val map: RectMapWithJungle): AbstractWorldMapElement(){

    var orientation: MapDirection = MapDirection.NORTH

    override var position: Vector2d = Vector2d(0,0)

    var energyTotal: Int = 0
    var energyMoveCost: Int = 0
    var genome: Genome = Genome()
    var age: Int = 0
    var childrenCount: Int = 0
    val children: MutableList<Animal> = mutableListOf()
    var deathDay: Int = -1


    private val uniqueId: Int = DateTimeFormatter.ISO_INSTANT.format(Instant.now()).hashCode()

    constructor(map: RectMapWithJungle, pos: Vector2d, ori: MapDirection, startEnergy: Int, moveCost: Int) : this(map){
        position = pos
        orientation = ori
        energyTotal = startEnergy
        energyMoveCost = moveCost
    }

    constructor(map: RectMapWithJungle, pos: Vector2d, ori: MapDirection, startEnergy: Int, moveCost: Int, genomeArg: Genome) : this(map){
        position = pos
        orientation = ori
        energyTotal = startEnergy
        energyMoveCost = moveCost
        genome = genomeArg
    }

    override fun toString(): String {
        return orientation.toString()
    }

    fun move() {
        val randomChoice: Int = genome.getDecision()
        val direction = when (randomChoice) {
            0 -> MoveDirection.FORWARD
            1 -> MoveDirection.QUARTER_RIGHT
            2 -> MoveDirection.RIGHT
            3 -> MoveDirection.THREE_QUARTERS_RIGHT
            4 -> MoveDirection.BACKWARD
            5 -> MoveDirection.THREE_QUARTERS_LEFT
            6 -> MoveDirection.LEFT
            7 -> MoveDirection.QUARTER_LEFT
            else -> throw IllegalArgumentException("Illegal direction (not in range 0-7)") // should never happen
        }
        when (direction) {
            MoveDirection.FORWARD ->  map.processMove(this, position, position + orientation.toUnitVector(), orientation, orientation)
            MoveDirection.QUARTER_RIGHT -> map.processMove(this, position, position, orientation, MapDirection.fromInt((orientation.dir + 1) % 8))
            MoveDirection.RIGHT -> map.processMove(this, position, position, orientation, MapDirection.fromInt((orientation.dir + 2) % 8))
            MoveDirection.THREE_QUARTERS_RIGHT -> map.processMove(this, position, position, orientation, MapDirection.fromInt((orientation.dir + 3) % 8))
            MoveDirection.BACKWARD ->  map.processMove(this, position, position - orientation.toUnitVector(), orientation, orientation)
            MoveDirection.THREE_QUARTERS_LEFT -> map.processMove(this, position, position, orientation, MapDirection.fromInt((orientation.dir + 5) % 8))
            MoveDirection.LEFT -> map.processMove(this, position, position, orientation, MapDirection.fromInt((orientation.dir + 6) % 8))
            MoveDirection.QUARTER_LEFT -> map.processMove(this, position, position, orientation, MapDirection.fromInt((orientation.dir + 7) % 8))
        }
    }

    fun isAlive(): Boolean = energyTotal > 0

    // recursive function to get descendents
    fun allDescendentsCount(): Int {
        var descendentsCount: Int = childrenCount

        for(child in children) {
            descendentsCount += child.allDescendentsCount()
        }
        return descendentsCount
    }

    override fun equals(other: Any?): Boolean {
        return this.hashCode() == other.hashCode()
    }

    override fun hashCode(): Int {
        var result = orientation.hashCode()
        result = 31 * result + position.hashCode() + uniqueId
        return result
    }
}