package agh.cs.oop.mapElements


class Genome() {
    // initialize genome to be only zeroes
    var genomeList: MutableList<Int> = MutableList(32) {0}

    init {
        // filling genome with random numbers and sorting if it is empty

        for(i in 0..31){
            genomeList[i] = listOf(0,1,2,3,4,5,6,7).random()
        }
        genomeList.sort()
    }

    constructor(genome: MutableList<Int>) : this(){
        genomeList = genome
    }

    fun getDecision(): Int {
        return genomeList.random()
    }

    fun mixWithOther(other: Genome, ratioThisToOther: Double): Genome {
        val thisGoLeft: Boolean = listOf(true, false).random()

        if(thisGoLeft){
            val pivot: Int = (ratioThisToOther/(ratioThisToOther + 1)).toInt()

            return Genome((this.genomeList.slice(IntRange(0, pivot - 1)) + other.genomeList.slice(IntRange(pivot, 31))).sorted() as MutableList<Int>)

        } else { // this genome go to Right
            val pivot: Int = (1 - ratioThisToOther/(ratioThisToOther + 1)).toInt()

            return Genome((other.genomeList.slice(IntRange(pivot, 31)) + this.genomeList.slice(IntRange(0, pivot - 1))).sorted() as MutableList<Int>)
        }
    }
}