package agh.cs.oop.mapElements

import agh.cs.oop.utils.Vector2d

class Grass(override val position: Vector2d, val energyWhenConsumed: Int): AbstractWorldMapElement() {
    override fun toString(): String {
        return "*"
    }
}