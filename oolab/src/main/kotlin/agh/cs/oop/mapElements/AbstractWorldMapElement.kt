package agh.cs.oop.mapElements

import agh.cs.oop.utils.Vector2d

abstract class AbstractWorldMapElement {
    abstract val position: Vector2d
}