package agh.cs.oop.gui

import agh.cs.oop.simulation.EngineWrapper
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
@Preview
@OptIn(ExperimentalFoundationApi::class)
fun StartMenu(game: EngineWrapper) {
    MaterialTheme {
        Column(modifier = Modifier.size(1000.dp,1000.dp).padding(10.dp))    {

            val numberOfStartingAnimals = remember { mutableStateOf(30) }
            val width = remember { mutableStateOf(16) }
            val height = remember { mutableStateOf(16) }
            val jungleRatio = remember { mutableStateOf(0.5) }
            val startAnimalEnergy = remember { mutableStateOf(100) }
            val energyMoveCost = remember { mutableStateOf(3) }
            val grassEnergy = remember { mutableStateOf(80) }
            val doEdgeWrap = remember { mutableStateOf(game.doEdgeWrap) }
            val magicEvolution = remember { mutableStateOf(true) }

            Button(onClick = {
                game.started = !game.started
                if (game.started) {
                    game.numberOfStartingAnimals = numberOfStartingAnimals.value
                    game.width = width.value
                    game.height = height.value
                    game.jungleRatio = jungleRatio.value
                    game.startAnimalEnergy = startAnimalEnergy.value
                    game.energyMoveCost = energyMoveCost.value
                    game.grassEnergy = grassEnergy.value
                    game.doEdgeWrap = doEdgeWrap.value
                    game.magicEvolution = magicEvolution.value

                    game.start()
                }
            }) {
                Text(if (game.started) "Stop" else "Start", fontSize = 40.sp)
            }

            ParamTextField("Startowa liczba zwierząt", numberOfStartingAnimals as MutableState<Any>) { it -> it.toInt() }
            ParamTextField("Szerokość mapy", width as MutableState<Any>) { it -> it.toInt() }
            ParamTextField("Wysokość mapy", height as MutableState<Any>) { it -> it.toInt() }
            ParamTextField("Proporcja dżugli do całości", jungleRatio as MutableState<Any>) { it -> it.toDouble() }
            ParamTextField("Startowa energia zwierzęcia", startAnimalEnergy as MutableState<Any>) { it -> it.toInt() }
            ParamTextField("Koszt ruchu zwierzęcia", energyMoveCost as MutableState<Any>) { it -> it.toInt() }
            ParamTextField("Energia za zjedzenie trawy", grassEnergy as MutableState<Any>) { it -> it.toInt() }
            Text("Zawijanie krawędzi")
            Checkbox(
                checked = doEdgeWrap.value,
                onCheckedChange = { doEdgeWrap.value = it }
            )
            Text("\"Magiczna\" ewolucja")
            Checkbox(
                checked = magicEvolution.value,
                onCheckedChange = { magicEvolution.value = it }
            )



        }
    }
}

@Composable
@Preview
@OptIn(ExperimentalFoundationApi::class)
fun ParamTextField(label: String, param: MutableState<Any>, convertFunc: (String) -> Any ) {
    Text(label)
    TextField(
        value = param.value.toString(),
        onValueChange = {
            if(it != "") {
                param.value = convertFunc(it)
            } else {
                param.value = 0
            }
        }
    )
}
