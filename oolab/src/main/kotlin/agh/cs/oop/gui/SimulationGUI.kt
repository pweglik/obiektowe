package agh.cs.oop.gui

import agh.cs.oop.mapElements.AbstractWorldMapElement
import agh.cs.oop.mapElements.Animal
import agh.cs.oop.mapElements.Grass
import agh.cs.oop.simulation.EngineWrapper
import androidx.compose.foundation.Image
import agh.cs.oop.utils.Vector2d
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.mouseClickable
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.util.*

@Composable
@Preview
@OptIn(ExperimentalFoundationApi::class)
fun SimulationGUI(simulationID: Int) {
    val game = remember { EngineWrapper(simulationID) }
    val darkBgResource = painterResource("dark_bg.png")
    val lightBgResource = painterResource("light_bg.png")
    val animalResource = painterResource("up.png")
    val grassResource = painterResource("grass.png")

    MaterialTheme() {
        if(!game.started) {

            StartMenu(game)

        } else { // game already started
            Column(modifier = Modifier.size(1000.dp, 1000.dp)) {
                Row(modifier = Modifier.size(1000.dp, 100.dp).padding(10.dp)) {
                    Text("Days ${game.dayCount}", fontSize = 35.sp)

                    // Pause/Resume button
                    Spacer(Modifier.padding(5.dp))
                    Button(onClick = {
                        game.togglePause()
                    }) {
                        Text(if (game.paused) "Resume" else "Pause", fontSize = 40.sp)
                    }
                }
                //end of top row

                // coroutine running the main loop
                LaunchedEffect(Unit) {
                    while (true) {
                        withFrameNanos {
                            if (game.started && !game.paused && !game.finished)
                                game.update()
                                if(game.engine.map.animals().isEmpty() || game.engine.magicSaves > 3) {
                                    game.finish()
                                }
                        }
                    }
                }


                Row(modifier = Modifier.size(1000.dp, 700.dp)) {
                    val n: Int = game.width
                    LazyVerticalGrid(
                        modifier = Modifier.size(500.dp, 500.dp),
                        cells = GridCells.Fixed(n)
                    ) {
                        items(game.width * game.height) { i ->
                            Box() {
                                val tempVector2d: Vector2d = Vector2d(
                                    i % game.width,
                                    -(i / game.width) + game.height - 1
                                )
                                val mapElementsFromTile: SortedSet<AbstractWorldMapElement>? =
                                    game.engine.map.mapElements[tempVector2d]
                                val mapElement: AbstractWorldMapElement

                                if (mapElementsFromTile == null || mapElementsFromTile.size == 0) {

                                    // checking if jungle
                                    if (tempVector2d.precedes(game.engine.map.jungleUpperRight)
                                        && tempVector2d.follows(game.engine.map.jungleLowerLeft)) {
                                        Image(
                                            painter = darkBgResource,
                                            contentDescription = "img",
                                            modifier = Modifier.fillMaxSize(),
                                            contentScale = ContentScale.Fit
                                        )
                                    } else {
                                        Image(
                                            painter = lightBgResource,
                                            contentDescription = "img",
                                            modifier = Modifier.fillMaxSize(),
                                            contentScale = ContentScale.Fit
                                        )
                                    }

                                } else {
                                    mapElement = mapElementsFromTile.last()
                                    if (mapElement is Animal) {
                                        Image(
                                            painter = animalResource,
                                            contentDescription = "img",
                                            modifier = Modifier.fillMaxSize()
                                                .mouseClickable {game.trackedAnimal = mapElement}
                                                .background(
                                                    if (tempVector2d.precedes(game.engine.map.jungleUpperRight)
                                                        && tempVector2d.follows(game.engine.map.jungleLowerLeft)) {
                                                        Color(146,144,144)
                                                    }
                                                    else {
                                                        Color(186,176,176)
                                                    }
                                                ),
                                            contentScale = ContentScale.Fit,
                                            alpha = minOf(mapElement.energyTotal.toFloat()/(game.startAnimalEnergy), 1f)
                                        )


                                    } else if (mapElement is Grass) {
                                        Image(
                                            painter = grassResource,
                                            contentDescription = "img",
                                            modifier = Modifier.fillMaxSize()
                                                .background(
                                                    if (tempVector2d.precedes(game.engine.map.jungleUpperRight)
                                                        && tempVector2d.follows(game.engine.map.jungleLowerLeft)) {
                                                        Color(146,144,144)
                                                    }
                                                    else {
                                                        Color(186,176,176)
                                                    }
                                                ),
                                            contentScale = ContentScale.Fit
                                        )
                                    }
                                }
                            }
                        }
                    }
                    // table with data instead of graphs as Jetpack Compose doesn't have a proper tool yet
                    Table(game.csvSaver)

                }
                Row(modifier = Modifier.size(800.dp, 200.dp)) {
                    BottomPanelGUI(game)
                }
            }
        }
    }
}
