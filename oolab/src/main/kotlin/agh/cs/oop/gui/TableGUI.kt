package agh.cs.oop.gui

import agh.cs.oop.utils.CSVSaver
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

// one table cell
@Composable
fun RowScope.TableCell(
    text: String
) {
    Text(
        text = text,
        Modifier
            .border(1.dp, Color.Black)
            .padding(2.dp)
            .size(56.dp,50.dp)
    )
}

// whole table
@Composable
@Preview
@OptIn(ExperimentalFoundationApi::class)
fun Table(csvSaver: CSVSaver) {

    // The LazyColumn will be our table. Notice the use of the weights below
    LazyColumn(Modifier.size(360.dp,700.dp)) {
        // Here is the header
        item{
            Row(Modifier.background(Color.Gray).size(500.dp,50.dp)) {
                TableCell(text = "#Day")
                TableCell(text = "#Ani")
                TableCell(text = "#Grass")
                TableCell(text = "AvgEn")
                TableCell(text = "AvgLT")
                TableCell(text = "AvgCh")
            }
        }

        //create rows with data
        for(i in (csvSaver.dayCount - 1) downTo 0) {
            item {
                Row(Modifier.fillMaxWidth()) {
                    TableCell(text = i.toString())
                    TableCell(text = csvSaver.animalsCountHistory[i].toString())
                    TableCell(text = csvSaver.grassCountHistory[i].toString())
                    TableCell(text = csvSaver.averageEnergyHistory[i].toString())
                    TableCell(text = csvSaver.averageTimeOfLifeHistory[i].toString())
                    TableCell(text = csvSaver.averageNumberOfChildrenHistory[i].toString())
                }
            }
        }
    }
}