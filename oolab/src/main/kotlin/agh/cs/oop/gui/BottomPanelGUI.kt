package agh.cs.oop.gui

import agh.cs.oop.simulation.EngineWrapper
import agh.cs.oop.utils.Statistics
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
@Preview
@OptIn(ExperimentalFoundationApi::class)
fun BottomPanelGUI(game: EngineWrapper) {
    Column(modifier = Modifier
        .padding(5.dp)) {
        val stats: Statistics = Statistics(game)
        val animalCount = stats.getAnimalCount()

        if(animalCount > 0) {
            Card() {
                val (dominantGenomeCount, genomeString) = stats.getDominantGenome()
                Text("Dominujący genom (w liczbie ${dominantGenomeCount}): ${genomeString}")
            }
        }
        if(game.trackedAnimal != null) {
            Card() {
                Text("Śledzone zwierze:")
            }
            Card() {
                Text("Genom: ${game.trackedAnimal!!.genome.genomeList}")
            }
            Card() {
                Text("Liczba dzieci: ${game.trackedAnimal!!.childrenCount}")
            }
            Card() {
                Text("Liczba potomków: ${game.trackedAnimal!!.allDescendentsCount()}")
            }
            Card() {
                if(game.trackedAnimal!!.isAlive()) {
                    Text("Zwierzę żyje!")
                }
                else {
                    if(game.trackedAnimal!!.deathDay == -1) {
                        game.trackedAnimal!!.deathDay = game.dayCount
                    }
                    Text("Zwierze zginęło w ${game.trackedAnimal!!.deathDay} dniu!")
                }
            }
        }
    }
}

