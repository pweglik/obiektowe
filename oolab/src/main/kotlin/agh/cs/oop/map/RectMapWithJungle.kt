package agh.cs.oop.map

import agh.cs.oop.mapElements.AbstractWorldMapElement
import agh.cs.oop.mapElements.Animal
import agh.cs.oop.mapElements.Genome
import agh.cs.oop.mapElements.Grass
import agh.cs.oop.utils.Vector2d
import java.util.*
import kotlin.collections.HashMap

class RectMapWithJungle(
    private val lowerLeft: Vector2d, private val upperRight: Vector2d,
    val jungleLowerLeft: Vector2d, val jungleUpperRight: Vector2d,
    private val doEdgesWrap: Boolean, private val grassEnergy: Int,
    private val startAnimalEnergy: Int, private val energyMoveCost: Int) {

    val mapElements: HashMap<Vector2d, SortedSet<AbstractWorldMapElement>> = hashMapOf()
    val deadAnimals: MutableList<Animal> = mutableListOf()

    private val visualizer: MapVisualizer = MapVisualizer(this)

    private val tileElementsComparator =  kotlin.Comparator { element1: AbstractWorldMapElement, element2: AbstractWorldMapElement ->
        if(element1 is Grass && element2 is Grass) return@Comparator 0
        if(element1 is Grass && element2 is Animal) return@Comparator -1
        if(element1 is Animal && element2 is Grass) return@Comparator 1

        if(element1 is Animal && element2 is Animal) {
            if(element1.hashCode() == element2.hashCode()) {
                return@Comparator 0
            }
            if (element1.energyTotal == element2.energyTotal) {
                if(element1.hashCode() < element2.hashCode()) {
                    return@Comparator -1
                } else {
                    return@Comparator 1
                }
            } else if (element1.energyTotal < element2.energyTotal) {
                return@Comparator -1
            } else {
                return@Comparator 1
            }
        } else {
            // should never happen but is required by comparator syntax for final branch
            return@Comparator 0
        }
    }

    // METHODS

    fun objectsAt(position: Vector2d): SortedSet<AbstractWorldMapElement>? {
        return mapElements[position]
    }

    fun animals(): List<Animal> {
        return mapElements.values.map { element ->
            element.toList().filterIsInstance<Animal>()
        }.fold(listOf()){ acc, list -> acc + list}
    }

    fun place(element: AbstractWorldMapElement): Boolean {
        if (mapElements[element.position] == null) {
            mapElements[element.position] = sortedSetOf(comparator = tileElementsComparator)
        }

        if(element is Animal) {
            mapElements[element.position]!!.add(element)
        }
        else if(element is Grass) {
            if (mapElements[element.position]!!.size == 0) {
                mapElements[element.position]!!.add(element)
            }
            else {
                return false
            }
        }
        return true

    }

    // Methods called in daily loop

    fun removeDeadAnimals() {
        this.animals().filter{ animal -> !animal.isAlive() }.map{ animal ->
            deadAnimals.add(animal)
            mapElements[animal.position]!!.remove(animal)

            if(mapElements[animal.position]!!.size == 0) {
                mapElements.remove(animal.position)
            }
        }
    }

    fun magicSpawn() {
        val freePositions: MutableList<Vector2d> = mutableListOf()

        for(x in this.lowerLeft.x..this.upperRight.x) {
            for(y in this.lowerLeft.y..this.upperRight.y) {
                val curVec = Vector2d(x,y)
                if (this.mapElements[curVec] == null) {
                    freePositions.add(curVec)
                }

            }
        }
        val animals = this.animals()
        for(animal in animals) {
            val randomPos = freePositions.random()
            freePositions.remove(randomPos)
            val randomOrientation: MapDirection = MapDirection.fromInt(listOf(0,1,2,3,4,4,5,6,7).random())
            val newAnimal = Animal(this, randomPos, randomOrientation, startAnimalEnergy, energyMoveCost, animal.genome)

            this.place(newAnimal)
        }
    }

    fun processMove(animal: Animal, previousPosition: Vector2d, newPosition: Vector2d,
                    previousOrientation: MapDirection, newOrientation: MapDirection) {

        var finalNewPosition: Vector2d = newPosition

        if(previousPosition != newPosition)
            if (this.doEdgesWrap) {
                if ( !(newPosition.precedes(upperRight) && newPosition.follows(lowerLeft)) ) {
                    val tempPos: Vector2d = newPosition + upperRight
                    val newX: Int = tempPos.x % upperRight.x
                    val newY: Int = tempPos.y % upperRight.y

                    finalNewPosition = Vector2d(newX, newY)
                }

            } else if ( !(newPosition.precedes(upperRight) && newPosition.follows(lowerLeft)) ) {
                finalNewPosition = previousPosition
            }

        mapElements[previousPosition]!!.remove(animal)
        if (mapElements[previousPosition]!!.size == 0) {
            mapElements.remove(previousPosition)
        }
        if (mapElements[finalNewPosition] == null) {
            mapElements[finalNewPosition] = sortedSetOf(comparator = tileElementsComparator)
        }
        // change fields of animal between removing and adding it to the set
        animal.energyTotal -= animal.energyMoveCost
        animal.position = finalNewPosition
        animal.orientation = newOrientation
        animal.age += 1

        mapElements[finalNewPosition]!!.add(animal)

    }

    fun processEating() {
        // find positions where grass is present
        val grassPositionsWithAnimals: List<Vector2d> = mapElements.filter{ setOfElements ->
            setOfElements.value.size >= 2 && setOfElements.value.first() is Grass && setOfElements.value.last() is Animal
        }.keys.toList()
        for(position in grassPositionsWithAnimals) {
            // get all animals with maximal energy to split the grass
            val maxEnergy: Int = (mapElements[position]!!.last() as Animal).energyTotal
            val eatingAnimals: MutableList<Animal> = mutableListOf()

            for(element in mapElements[position]!!) {
                if(element is Animal && element.energyTotal == maxEnergy) {
                    eatingAnimals.add(element)
                }
            }
            // increase animals energy
            eatingAnimals.map{ animal ->
                animal.energyTotal += (mapElements[position]!!.first() as Grass).energyWhenConsumed / (mapElements[position]!!.size - 1)
            }
            // remove grass
            mapElements[position]!!.remove(mapElements[position]!!.first())

        }
    }

    fun processBreeding() {
        // find positions where two animals are present
        val positionsWithTwoAnimals: List<Vector2d> = mapElements.filter{ setOfElements ->
            setOfElements.value.size >= 2 && setOfElements.value.count{ element -> element is Animal} >= 2
        }.keys.toList()
        for(position in positionsWithTwoAnimals) {
            // get two parents
            val parent1: Animal = mapElements[position]!!.last() as Animal
            val parent2: Animal = mapElements[position]!!.last() as Animal

            // create new Animal if parents fulfill conditions

            if(parent1.energyTotal > startAnimalEnergy/2 && parent2.energyTotal > startAnimalEnergy/2) {
                mapElements[position]!!.remove(parent1)
                mapElements[position]!!.remove(parent2)

                val newGenome: Genome = parent1.genome.mixWithOther(parent2.genome, parent1.energyTotal.toDouble()/parent2.energyTotal.toDouble())
                val randomOrientation: MapDirection = MapDirection.fromInt(listOf(0,1,2,3,4,4,5,6,7).random())
                val newAnimal = Animal(this, position, randomOrientation, startAnimalEnergy, energyMoveCost, newGenome)

                // overwrite current energy and decrease energy of parents
                newAnimal.energyTotal = parent1.energyTotal / 4 + parent2.energyTotal / 4
                parent1.energyTotal = 3 * parent1.energyTotal / 4
                parent2.energyTotal = 3 * parent2.energyTotal / 4
                parent1.childrenCount += 1
                parent2.childrenCount += 1
                parent1.children.add(newAnimal)
                parent2.children.add(newAnimal)

                this.place(newAnimal)
                this.place(parent1)
                this.place(parent2)
            }

        }
    }

    fun spawnDailyGrass() {
        val savannaFreeTilePositions: MutableList<Vector2d> = mutableListOf()
        val jungleFreeTilesPositions: MutableList<Vector2d> = mutableListOf()

        val lowX: Int = this.lowerLeft.x
        val lowY: Int = this.lowerLeft.y
        val highX: Int = this.upperRight.x
        val highY: Int = this.upperRight.y

        for(x in lowX..highX) {
            for(y in lowY..highY) {
                val curVec = Vector2d(x,y)
                if (curVec.precedes(jungleUpperRight) && curVec.follows(jungleLowerLeft)) {
                    if (this.mapElements[curVec] == null) {
                        jungleFreeTilesPositions.add(curVec)
                    }
                } else {
                    if (this.mapElements[curVec] == null) {
                        savannaFreeTilePositions.add(curVec)
                    }
                }
            }
        }
        if (jungleFreeTilesPositions.size > 0) {
            val jungleTilePosition: Vector2d = jungleFreeTilesPositions.random()
            this.place(Grass(jungleTilePosition, grassEnergy))
        }

        if (savannaFreeTilePositions.size > 0) {
            val savannaTilePosition: Vector2d = savannaFreeTilePositions.random()
            this.place(Grass(savannaTilePosition, grassEnergy))
        }
    }

    override fun toString():String {
        return visualizer.draw(lowerLeft, upperRight)
    }
}
