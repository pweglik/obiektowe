package agh.cs.oop.map

import agh.cs.oop.utils.Vector2d

enum class MapDirection(val outputString: String, val dir: Int) {
    NORTH("^",0),
    NORTHEAST("^>",1),
    EAST(">",2),
    SOUTHEAST("v>",3),
    SOUTH("v", 4),
    SOUTHWEST("v<", 5),
    WEST("<", 6),
    NORTHWEST("^<", 7);

    companion object {
        fun fromInt(value: Int) = MapDirection.values().first { it.dir == value }
    }

    override fun toString(): String {
        return outputString
    }

    fun toUnitVector(): Vector2d {
        return when(this){
            NORTH -> Vector2d(0,1)
            NORTHEAST -> Vector2d(1,1)
            WEST -> Vector2d(-1,0)
            SOUTHEAST -> Vector2d(1,-1)
            SOUTH -> Vector2d(0,-1)
            SOUTHWEST -> Vector2d(-1,-1)
            EAST -> Vector2d(1,0)
            NORTHWEST -> Vector2d(-1,1)
        }
    }


}