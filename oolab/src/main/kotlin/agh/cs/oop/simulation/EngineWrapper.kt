package agh.cs.oop.simulation

import agh.cs.oop.mapElements.Animal
import agh.cs.oop.utils.CSVSaver
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue

class EngineWrapper(simulationID: Int) {

    // game states
    var started by mutableStateOf(false)
    var paused by mutableStateOf(false)
    var finished by mutableStateOf(false)

    // rest of states
    var dayCount by mutableStateOf(0)

    // engine init
    // those are default values that can be changed without GUI
    // they are overwritten by StartMenu when starting simulation
    var numberOfStartingAnimals: Int = 30
    var width: Int = 16
    var height: Int= 16
    var jungleRatio: Double = 0.5
    var startAnimalEnergy: Int = 150
    var energyMoveCost: Int = 4
    var grassEnergy: Int= 80
    var doEdgeWrap: Boolean = false
    var magicEvolution: Boolean = false

    lateinit var engine: SimulationEngine
    lateinit var csvSaver: CSVSaver
    var trackedAnimal: Animal? = null

    init {
        // first map - with wrapped edges, second with walls
        if(simulationID == 1) {
            doEdgeWrap = true
            csvSaver = CSVSaver(this, "simulation_1.csv")
        } else if(simulationID == 2) {
            doEdgeWrap = false
            csvSaver = CSVSaver(this, "simulation_2.csv")
        }
    }




    fun start() {
        started = true
        finished = false
        paused = true

        engine = SimulationEngine(numberOfStartingAnimals,
            width, height, jungleRatio,
            startAnimalEnergy,
            energyMoveCost,
            grassEnergy,
            doEdgeWrap,
            magicEvolution,
            csvSaver)
    }

    fun togglePause() {
        paused = !paused
        if(paused) csvSaver.saveDataToCSV()
    }

    fun update() {
        // update code
        dayCount += 1
        engine.runStep()
    }

    fun finish(){
        paused = true
        finished = true
        csvSaver.saveDataToCSV()
    }

}