package agh.cs.oop.simulation

import agh.cs.oop.map.MapDirection
import agh.cs.oop.map.RectMapWithJungle
import agh.cs.oop.mapElements.Animal
import agh.cs.oop.utils.CSVSaver
import agh.cs.oop.utils.Vector2d
import kotlin.math.sqrt

class SimulationEngine(numberOfStartingAnimals: Int,
                       width: Int, height: Int, jungleRatio: Double,
                       startAnimalEnergy: Int,
                       energyMoveCost: Int,
                       grassEnergy: Int,
                       doEdgeWrap: Boolean,
                       val magicEvolution: Boolean,
                       val csvSaver: CSVSaver) {


    var map: RectMapWithJungle
    var magicSaves: Int = 0
    init {
        // create map from application input data
        val jungleSideLength: Int = sqrt(width * height * jungleRatio).toInt()

        map = RectMapWithJungle(
                Vector2d(0,0),
                Vector2d(width, height),
                Vector2d((width - jungleSideLength)/2,(height - jungleSideLength)/2),
                Vector2d((width + jungleSideLength)/2,(height + jungleSideLength)/2),
                doEdgeWrap, grassEnergy,
                startAnimalEnergy, energyMoveCost)

        // generate random starting animals
        for(i in 0 until numberOfStartingAnimals) {
            while(true) {
                val randomX = (0..width).random()
                val randomY = (0..height).random()
                val position = Vector2d(randomX, randomY)
                val orientation: MapDirection = MapDirection.fromInt(listOf<Int>(0,1,2,3,4,4,5,6,7).random())
                if(map.mapElements[position] == null) {
                    val newAnimal = Animal(map, position, orientation, startAnimalEnergy, energyMoveCost)
                    map.place(newAnimal)
                    break
                }
            }
        }
    }

    fun runStep() {

        // Removing dead animals
        map.removeDeadAnimals()

        //check if any niamlas survived
        if(map.animals().isNotEmpty()) {
            //potential magic spawning
            if(magicEvolution && map.animals().size <= 5) {
                map.magicSpawn()
                magicSaves += 1
            }

            // Moving animals
            for(animal in map.animals()) {
                animal.move()
            }

            // Eating plants
            map.processEating()

            // Creating new animals (first you need two animals that love each other quite a lot
            map.processBreeding()

            // Spawning grass
            map.spawnDailyGrass()

            // Saving statistics to csv files
            csvSaver.saveDay()
        }
    }
}