package agh.cs.oop.utils

import agh.cs.oop.mapElements.Grass
import agh.cs.oop.simulation.EngineWrapper

class Statistics(private val game: EngineWrapper) {

    fun getAnimalCount(): Int = game.engine.map.animals().size

    fun getDominantGenome(): Pair<String, Int> {
        val listOfGenomesLists: MutableList<MutableList<Int>> = mutableListOf()

        game.engine.map.animals().map { animal ->
            listOfGenomesLists.add(animal.genome.genomeList)
        }

        val genomeList: MutableList<Int> = listOfGenomesLists.groupingBy{ it }.eachCount().maxByOrNull{ it.value }?.key!!
        val dominantGenomeCount: Int = listOfGenomesLists.groupingBy{ it }.eachCount().maxByOrNull{ it.value }?.value!!
        return genomeList.joinToString(separator = ",") to dominantGenomeCount
    }

    fun getAllAnimalsTotalEnergy(): Int {
        var allAnimalsTotalEnergy = 0
        game.engine.map.animals().map{animal ->
            allAnimalsTotalEnergy += animal.energyTotal
        }
        return allAnimalsTotalEnergy
    }

    fun getAverageNumberOfChildren(animalCount: Int): Float {
        var totalChildren = 0

        game.engine.map.animals().map{animal ->
            totalChildren += animal.childrenCount
        }

        return (totalChildren.toDouble()/animalCount.toDouble() * 100).toInt() / 100f
    }

    fun getGrassCount(): Int {
        var grassCount = 0
        game.engine.map.mapElements.values.map{ setOfElements ->
            grassCount += setOfElements.count{ element -> element is Grass }
        }
        return grassCount
    }

    fun getAverageTimeOfLife(): Int? {
        var totalDeadAnimalAge = 0
        val deadAnimalCount: Int = game.engine.map.deadAnimals.size

        game.engine.map.deadAnimals.map{deadAnimal ->
            totalDeadAnimalAge += deadAnimal.age
        }
        return if(deadAnimalCount != 0) totalDeadAnimalAge/deadAnimalCount
        else null
    }


}