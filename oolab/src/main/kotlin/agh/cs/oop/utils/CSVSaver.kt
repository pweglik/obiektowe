package agh.cs.oop.utils

import agh.cs.oop.simulation.EngineWrapper
import java.io.File


// The idea is to save everything on pausing or stopping animation
class CSVSaver(game: EngineWrapper, private val filename: String) {
//    liczby wszystkich zwierząt,
//    liczby wszystkich roślin,
//    średniego poziomu energii dla żyjących zwierząt,
//    średniej długości życia zwierząt dla martwych zwierząt (wartość uwzględnia wszystkie nieżyjące zwierzęta - od początku symulacji),
//    średniej liczby dzieci dla żyjących zwierząt (wartość uwzględnia wszystkie powstałe zwierzęta, a nie tylko zwierzęta powstałe w danej epoce).

    private val stats: Statistics = Statistics(game)

    var dayCount: Int = 0
    val animalsCountHistory: MutableList<Int> = mutableListOf()
    val grassCountHistory: MutableList<Int> = mutableListOf()
    val averageEnergyHistory: MutableList<Int> = mutableListOf()
    val averageTimeOfLifeHistory: MutableList<Int?> = mutableListOf()
    val averageNumberOfChildrenHistory: MutableList<Float> = mutableListOf()

    fun saveDay() {
        animalsCountHistory.add(stats.getAnimalCount())
        grassCountHistory.add(stats.getGrassCount())
        averageEnergyHistory.add(stats.getAllAnimalsTotalEnergy()/stats.getAnimalCount())
        averageTimeOfLifeHistory.add(stats.getAverageTimeOfLife())
        averageNumberOfChildrenHistory.add(stats.getAverageNumberOfChildren(stats.getAnimalCount()))

        dayCount += 1
    }

    fun saveDataToCSV() {
        var stringToSave = "day;animals_count;grass_count;average_energy;average_lifetime;average_child_count\n"


        for(i in 0 until dayCount) {
            stringToSave += "${i};${animalsCountHistory[i]};${grassCountHistory[i]};${averageEnergyHistory[i]};" +
                    "${averageTimeOfLifeHistory[i]};${averageNumberOfChildrenHistory[i]}\n"
        }

        stringToSave += "AVERAGE_FOR_ALL_DAYS;" +
                "${animalsCountHistory.average()};${grassCountHistory.average()};${averageEnergyHistory.average()};" +
                "${averageTimeOfLifeHistory.filterNotNull().average()};${averageNumberOfChildrenHistory.average()}\n"

        File(filename).printWriter().use { out ->
            out.write(stringToSave)
        }
    }

}