package agh.cs.oop.utils

import kotlin.math.max
import kotlin.math.min

data class Vector2d(val x: Int, val y: Int) {
    override fun toString(): String {
        return "($x, $y)"

    }
    fun precedes(other: Vector2d): Boolean {
        return x <= other.x && y <= other.y
    }
    fun follows(other: Vector2d): Boolean {
        return x >= other.x && y >= other.y
    }
    fun upperRight(other: Vector2d): Vector2d {
        return Vector2d(max(x, other.x), max(y, other.y))
    }
    fun lowerLeft(other: Vector2d): Vector2d {
        return Vector2d(min(x, other.x), min(y, other.y))
    }
    operator fun plus(other: Vector2d): Vector2d {
        return Vector2d(x + other.x, y + other.y)
    }
    operator fun minus(other: Vector2d): Vector2d {
        return Vector2d(x - other.x, y - other.y)
    }
    override fun equals(other: Any?): Boolean {
        return other is Vector2d && other.x == x && other.y == y
    }
    fun opposite(): Vector2d {
        return Vector2d(-x, -y)
    }

}