package agh.cs.oop

import agh.cs.oop.utils.Vector2d
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class Vector2dTest {
    @Test
    fun testToString() {
        assertEquals("(2, 3)",  Vector2d(2,3).toString())
        assertEquals("(-1, 2)", Vector2d(-1,2).toString())
    }

    @Test
    fun precedes() {
        assertEquals(true,  Vector2d(1,1).precedes( Vector2d(2,3)))
        assertEquals(false,  Vector2d(1,1).precedes(Vector2d(-1,2)))
    }

    @Test
    fun follows() {
        assertEquals(false,  Vector2d(2,2).follows( Vector2d(2,3)))
        assertEquals(true,  Vector2d(2,2).follows(Vector2d(-1,2)))
    }

    @Test
    fun upperRight() {
        assertEquals(Vector2d(4,3), Vector2d(4,2).upperRight( Vector2d(2,3)))
        assertEquals(Vector2d(1,2), Vector2d(1,1).upperRight(Vector2d(-1,2)))
    }

    @Test
    fun lowerLeft() {
        assertEquals(Vector2d(2,2), Vector2d(4,2).lowerLeft( Vector2d(2,3)))
        assertEquals(Vector2d(-1,1), Vector2d(1,1).lowerLeft(Vector2d(-1,2)))
    }

    @Test
    fun add() {
        assertEquals(Vector2d(6,5), Vector2d(4,2) +  Vector2d(2,3))
        assertEquals(Vector2d(0,3), Vector2d(1,1) + Vector2d(-1,2))
    }

    @Test
    fun subtract() {
        assertEquals(Vector2d(2,-1), Vector2d(4,2) -  Vector2d(2,3))
        assertEquals(Vector2d(2,-1), Vector2d(1,1) - Vector2d(-1,2))
    }

    @Test
    fun testEquals() {
        assertEquals(true, Vector2d(2,3) ==  Vector2d(2,3))
        assertEquals(false, Vector2d(1,2) == Vector2d(-1,2))
    }

    @Test
    fun opposite() {
        assertEquals(Vector2d(-2,-3),  Vector2d(2,3).opposite())
    }
}