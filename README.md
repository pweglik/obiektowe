# README #

Laby z przedmiotu "Programowanie obiektowe"
Autor: Przemek Węglik

Projekt napisany w języku Kotlin. z wymagań nie zostały spełnione rysowanie wykresów ze względu na brak dogodnego narzędzia. Ogólne przemyślenia: Compose nie służy do tego typu rzeczy. Cała animacja powinna być zrobiona na canvasie.